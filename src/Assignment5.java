import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment5 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chrome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://qaclickacademy.com/practice.php");
		
		//enter three letters of the country
		driver.findElement(By.id("autocomplete")).sendKeys("uni");
		driver.findElement(By.id("autocomplete")).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(1000);
	
		int count=0;
		
		//create Javascript executor object to run javascript command
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String commandTogetText = "return document.getElementById(\"autocomplete\").value;";
		String textboxValue = (String) js.executeScript(commandTogetText);
		while(!(textboxValue.equals("Tunisia")))
		{
			count++;
			System.out.println(textboxValue);
			driver.findElement(By.id("autocomplete")).sendKeys(Keys.ARROW_DOWN);
			textboxValue = (String) js.executeScript(commandTogetText);
			Thread.sleep(1000);
			if(count>10)
			{
				break;
			}
		}
		
		if(count>10)
		{
			System.out.print("Could not find country");
		}
		else
		{
			System.out.print("Country found in dropdown");
		}
				
		

	}

}
