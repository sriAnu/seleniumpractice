import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestLinksinFooter {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chrome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://qaclickacademy.com/practice.php");
		
		WebElement footer = driver.findElement(By.id("gf-BIG"));
		
		System.out.print(footer.findElements(By.tagName("a")).size());
		//to get links in first column
		WebElement firstColumn= footer.findElement(By.xpath("//table/tbody/tr/td[1]/ul"));
		//System.out.println(firstColumnLnkCnt);
		String clickinnewtab = Keys.chord(Keys.CONTROL,Keys.ENTER);
		//get links in first column
		int firstColumnLnkCnt = firstColumn.findElements(By.tagName("a")).size();
		for(int i=0; i<firstColumnLnkCnt;i++) {
			firstColumn.findElements(By.tagName("a")).get(i).sendKeys(clickinnewtab);
		}
		
		Set<String> windowids = driver.getWindowHandles();
		Iterator<String> ids = windowids.iterator();
		while(ids.hasNext()) {
			System.out.println(driver.switchTo().window(ids.next()).getTitle());
		}
		
	}

}
