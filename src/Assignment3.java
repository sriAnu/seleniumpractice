import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Assignment3 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chrome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.itgeared.com/demo/1506-ajax-loading.html");
		
		driver.findElement(By.xpath("//a[contains(@href,'loadAjax()')]")).click();
		//Adding explicit wait
		WebDriverWait eWDriver = new WebDriverWait(driver,20);
		eWDriver.until(ExpectedConditions.elementToBeClickable(By.id("results")));
		Assert.assertEquals(driver.findElement(By.id("results")).getText(),"Process completed! This response has been loaded via the Ajax request directly from the web server, without reoading this page.");
	

	}

}
