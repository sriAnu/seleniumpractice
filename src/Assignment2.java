import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Assignment2 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chrome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.cleartrip.com/");
		//select 2 adults
		Select adults = new Select(driver.findElement(By.id("Adults")));
		adults.selectByValue("2");
	
		//select 2 children
		Select children = new Select(driver.findElement(By.id("Childrens")));
		children.selectByValue("2");
		
		//Depart on current date
		driver.findElement(By.id("DepartDate")).click();
		driver.findElement(By.cssSelector(".ui-state-default.ui-state-highlight.ui-state-active ")).click();
		//click on more options
		driver.findElement(By.id("MoreOptionsLink")).click();
		//enter preferred airline
		driver.findElement(By.id("AirlineAutocomplete")).sendKeys("indigo");
		//click on search button
		driver.findElement(By.id("SearchBtn")).click();
		Thread.sleep(2000);
		//print error
		System.out.println(driver.findElement(By.id("homeErrorMessage")).getText());
		
		
	}

}
