import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class Assignment4 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chrome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://qaclickacademy.com/practice.php");
		
		//select any checkbox
		driver.findElement(By.id("checkBoxOption3")).click();
		//get the label of that checkbox
		String chkboxLabel = driver.findElement(By.xpath("//input[@id='checkBoxOption3']/parent::label")).getText();
		System.out.println(chkboxLabel);
		
		//Select the chkboxLabel from dropdown
		Select drpdwn = new Select(driver.findElement(By.id("dropdown-class-example")));
		drpdwn.selectByVisibleText(chkboxLabel);
		
		//Enter chkboxLabel in the input box
		driver.findElement(By.id("name")).sendKeys(chkboxLabel);
		System.out.println(chkboxLabel);
		
		//Click on Alert button
		driver.findElement(By.id("alertbtn")).click();
		
		//get text from Alert box and check if it contains the chkboxLabel
		Assert.assertTrue(driver.switchTo().alert().getText().contains(chkboxLabel));
		driver.switchTo().alert().accept();
		
	}

}
