import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestUiSpiceJet {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","C:\\Program Files\\chrome\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://spicejet.com");
		//Select dropdown -currency
		Select s = new Select(driver.findElement(By.cssSelector("select#ctl00_mainContent_DropDownListCurrency")));
		s.selectByVisibleText("AED");
		
		//Dynamic dropdowns
		driver.findElement(By.cssSelector("div.row1.adult-infant-child")).click();
		//driver.findElement(By.cssSelector("span#hrefIncAdt.pax-add.pax-enabled")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("hrefIncAdt")).click();
		driver.findElement(By.cssSelector("span#hrefIncAdt.pax-add")).click();
		
		driver.findElement(By.id("btnclosepaxoption")).click();
		//driver.findElement(By.cssSelector("input.btnclosepaxoption.buttonN")).click();
		
		System.out.print(driver.findElement(By.id("divpaxinfo")).getText());
		
		Select origin = new Select(driver.findElement(By.id("ctl00_mainContent_ddl_originStation1")));
		origin.selectByVisibleText("Chennai (MAA)");
		driver.findElement(By.id("ctl00_mainContent_ddl_destinationStation1")).click();
		
	}
	

}
